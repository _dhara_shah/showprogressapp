# About the application #

This is a simple application that takes the number of times the image displayed needs to be stored into the database.
On doing so, a progress is shown to the user, so that the user can know the amount of data that has been stored.

# How to setup #

Install android studio and android sdk
Clone this repository or import it into studio
Run like every other application
The apk generated will be found in the folder app/build/outputs/apk/

# Dependencies #

No extra modules have been used in this application. The only libraries integrated using gradle are:
1. Appcompat library
2. Material edit text

# Contact #
In case of any queries contact *sdhara2@hotmail.com*