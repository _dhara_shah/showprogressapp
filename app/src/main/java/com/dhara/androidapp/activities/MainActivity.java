package com.dhara.androidapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dhara.androidapp.MyApp;
import com.dhara.androidapp.R;
import com.dhara.androidapp.asynctasks.InsertIntoDatabaseAsyncTask;
import com.rengwuxian.materialedittext.MaterialEditText;

public class  MainActivity extends AppCompatActivity {
    private MaterialEditText mEditNumberOfRows;
    private long mNumberOfRows;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    /**
     * Initializes the views
     */
    private void initView() {
        mEditNumberOfRows = (MaterialEditText)findViewById(R.id.edt_number_of_rows);
    }

    /**
     * Click event of the submit button
     * @param view
     */
    public void submitClicked(View view) {
        if(TextUtils.isEmpty(mEditNumberOfRows.getText().toString())) {
            Toast.makeText(MyApp.getAppContext(),
                    getString(R.string.please_enter_number), Toast.LENGTH_SHORT).show();
        }
        mNumberOfRows = Long.parseLong(mEditNumberOfRows.getText().toString());
        new InsertIntoDatabaseAsyncTask(MainActivity.this, mNumberOfRows).execute();
    }
}
