package com.dhara.androidapp.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;

import com.dhara.androidapp.MyApp;
import com.dhara.androidapp.R;
import com.dhara.androidapp.customdialogs.DialogUtils;
import com.dhara.androidapp.db.DBHelper;

import java.io.ByteArrayOutputStream;

/**
 * Created by USER on 30-04-2016.
 */
public class InsertIntoDatabaseAsyncTask extends AsyncTask<Void, Integer, Void> {
    private long mNumber;
    private Context mContext;
    private DBHelper mDbHelper;
    private ProgressDialog mProgressDialog;

    public InsertIntoDatabaseAsyncTask(Context context,long number){
        mNumber = number;
        mContext = context;
        mDbHelper = DBHelper.getInstance(MyApp.getAppContext());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = DialogUtils.getProgressDialog(mContext,mNumber);
        mProgressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        /**
         * Reading the image data and converting it into bytes to store it in the form of a blob
         */
        Drawable drawable_img = ContextCompat.getDrawable(MyApp.getAppContext(), R.drawable.nell_boots);
        Bitmap drawable_bitmap = ((BitmapDrawable)drawable_img).getBitmap();
        ByteArrayOutputStream outstream = new ByteArrayOutputStream();
        drawable_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outstream);

        byte[] imageBytes = outstream.toByteArray();

        int i =0;

        while(i < mNumber) {
            i++;
            mDbHelper.insertImage(imageBytes);

            /**
             * Displays the progress on the progress bar
             */
            if(mProgressDialog != null) {
                publishProgress((int) ((float) i / mNumber * 100));
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        mProgressDialog.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
