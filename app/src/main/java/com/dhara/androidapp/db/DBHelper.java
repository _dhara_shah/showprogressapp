package com.dhara.androidapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper
{
	private static DBHelper mDatabaseHelper;
	/**
	 * Create query for the images table
	 */
	private static final String SQL_CREATE_TABLE_IMAGES =
			"create table " + ImageContract.ImageEntry.TABLE_NAME +
			" ( " + 
			ImageContract.ImageEntry._ID + " INTEGER PRIMARY KEY, " +
			ImageContract.ImageEntry.COLUMN_NAME_IMAGE_BLOB + " BLOB " +
			" ) ";
	
	public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "images_db.sqlite";

	public static DBHelper getInstance(Context context){
		if(mDatabaseHelper == null) {
			mDatabaseHelper= new DBHelper(context);
		}
		return mDatabaseHelper;
	}
	
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_TABLE_IMAGES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

	/**
	 * Insert image into the database
	 * @param imageBytes
	 * @return
	 */
	public long insertImage(byte[] imageBytes) {
		SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(ImageContract.ImageEntry.COLUMN_NAME_IMAGE_BLOB, imageBytes);
	
		long id = db.insert(ImageContract.ImageEntry.TABLE_NAME, null, values);
		return id;
	}
 }
