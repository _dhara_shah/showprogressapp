package com.dhara.androidapp.db;

import android.provider.BaseColumns;

public class ImageContract {
	 public ImageContract() {}

	 /* Inner class that defines the table contents */
	 public static abstract class ImageEntry implements BaseColumns {
		 public static final String TABLE_NAME = "image_master";
		 public static final String COLUMN_NAME_IMAGE_BLOB = "imageblob";
	 }
}
