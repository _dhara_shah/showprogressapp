package com.dhara.androidapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by USER on 30-04-2016.
 */
public class MyApp extends Application{
    private static MyApp mApp;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        mContext = this;
    }

    public static MyApp getAppContext(){
        if(mApp == null) {
            mApp = (MyApp)mContext;
        }
        return mApp;
    }
}
