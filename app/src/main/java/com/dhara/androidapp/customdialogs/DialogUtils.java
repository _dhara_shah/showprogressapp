package com.dhara.androidapp.customdialogs;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by USER on 30-04-2016.
 */
public class DialogUtils {
    /**
     * Creates and returns an instance of the progress dialog
     * @param context
     * @param progress
     * @return
     */
    public static ProgressDialog getProgressDialog(Context context, long progress){
        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Saving images. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax((int)progress);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        return pDialog;
    }
}
